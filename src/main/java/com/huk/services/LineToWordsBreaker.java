package com.huk.services;

import com.huk.SongInfo;
import com.huk.Word;
import java.util.HashMap;
import java.util.Map;

public class LineToWordsBreaker {

    public Map<String, Word> getWords(SongInfo songInfo) {// брать строчки с песни и раскладывать слова
        Map<String, Word> stringWordMap = new HashMap<>();
        for (String line : songInfo.getLines()) {// возращает лист строк
            line = line.toLowerCase(); // делаем маленькими буквами
            for (String word : line.split("[!?:\\-.,\\s]+")) {// из строски выдергиваем слово
                if (word.equals("")) continue; //отсеиваем чтобы пустышки не считало словами
                if (stringWordMap.get(word) == null) {
                    stringWordMap.put(word, new Word(word));
                } else stringWordMap.get(word).count();
            }
        }
        return stringWordMap;
    }
}
