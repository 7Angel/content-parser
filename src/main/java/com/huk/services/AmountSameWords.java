package com.huk.services;

import com.huk.Word;
import com.huk.entities.SongStatisticEntity;

import java.util.Map;
//фильтруем те слова которые используются больше 1го раза в тексте
public class AmountSameWords implements WordFunction {
    @Override
    public SongStatisticEntity apply(Map<String, Word> stringWordMap, SongStatisticEntity songStatisticEntity) {
        Integer number = (int) stringWordMap.values()
                           .stream()
                           .filter(word -> word.getAmount() > 1)
                           .count();
        songStatisticEntity.setAmountSameWords(number);
        return songStatisticEntity;
    }
}
