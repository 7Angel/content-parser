//package com.huk.services;
//
//import com.huk.Word;
//import com.huk.entities.SongStatisticEntity;
//import org.junit.Test;
//import org.junit.jupiter.api.Assertions;
//
//import java.util.HashMap;
//import java.util.Map;
//
//
//public class AmountUniqueWordsTest {
//
//    private final WordFunction wordFunction = new AmountUniqueWords();
//
//    @Test
//    public void uniqueWordsAmountWillBeCalculated() {
//        Map<String, Word> wordsMap = new HashMap<>();
//        wordsMap.put("str1", new Word("str1"));
//        wordsMap.put("str2", new Word("str2"));
//
//        SongStatisticEntity statisticEntity = new SongStatisticEntity();
//
//        wordFunction.apply(wordsMap, statisticEntity);
//
//        Assertions.assertEquals(statisticEntity.getAmountUniqueWords(), 1);
//    }
//
//}