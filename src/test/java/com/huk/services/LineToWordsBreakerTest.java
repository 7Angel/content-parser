package com.huk.services;

import com.huk.SongInfo;
import com.huk.Word;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.Arrays;
import java.util.Map;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class LineToWordsBreakerTest {

    @InjectMocks
    private LineToWordsBreaker lineToWordsBreaker;

    @Mock
    private SongInfo songInfo;

    @Test
    public void mapOfWordsWillBeReturnedWhenGivenSongInfo(){
        Mockito.doReturn(Arrays.asList("First! Line,", "Second? line", "Third: - LINE...")).when(songInfo).getLines();

        Map<String, Word> words = lineToWordsBreaker.getWords(songInfo);

        assertNotNull(words);
        assertFalse(words.isEmpty());
        assertFalse(words.containsKey(""));
        assertFalse(words.containsKey("."));
        assertFalse(words.containsKey(" "));
        assertFalse(words.containsKey(","));
        assertFalse(words.containsKey("!"));
        assertFalse(words.containsKey("?"));
        assertFalse(words.containsKey("-"));
        assertFalse(words.containsKey(":"));
        assertTrue(words.containsKey("first"));
        assertTrue(words.containsKey("second"));
        assertTrue(words.containsKey("third"));
        assertTrue(words.containsKey("line"));
        Assertions.assertEquals(1, words.get("first").getAmount());
        Assertions.assertEquals(1, words.get("second").getAmount());
        Assertions.assertEquals(1, words.get("third").getAmount());
        Assertions.assertEquals(3, words.get("line").getAmount());

        Mockito.verify(songInfo).getLines();

}

//    private SongInfo songInfo;
//
//    @Test
//    public void stringWordMapWillReturnEmptyWhenStringInfoLinesIsEmpty() {
//
//        Map<String, Word> stringWordMap = new HashMap<>();
//        List<SongInfo> songInfo = new ArrayList<>();
//        Assertions.assertEquals(songInfo.size(), 0);
//        Assertions.assertEquals(stringWordMap.size(), 0);
//
//    }

//    @Test
//    public void stringWordMapWillReturnLowerCaseWhenStringInfoLinesIsNotEmpty() {
//        Map<String, Word> stringWordMap = new HashMap<>();
//        List<SongInfo> songInfo = new ArrayList<>();
//        songInfo.add(new SongInfo("name", singletonList("Mu dak lol Tu"), "url"));
//        when(lineToWordsBreaker.getWords(songInfo.get(0))).thenReturn(stringWordMap);
//
//        Assertions.assertEquals(songInfo.get(0).getLines().get(0), "Mu dak lol Tu");
//        Assertions.assertEquals(stringWordMap.get(0), null);
//
//    }


//    @Test
//    public void dummyWillBeDropOutWhenWordEqualsFoundIt() {
//        Map<String, Word> stringWordMap = new HashMap<>();
//        SongInfo songInfo = new SongInfo("name", singletonList(""), "url");
//        when(lineToWordsBreaker.getWords(songInfo)).thenReturn(stringWordMap);
//
//        Assertions.assertEquals(stringWordMap.size(),0);
//
//    }
//
//    @Test
//    public void songInfoWordWillBePutWhenMeetFirstTime(){
//        Map<String, Word> stringWordMap = new HashMap<>();
//        SongInfo songInfo = new SongInfo("name", singletonList("Мама"), "url");
//        when(lineToWordsBreaker.getWords(songInfo)).thenReturn(stringWordMap);
//
//        Assertions.assertEquals(stringWordMap.size(),0);
//    }
//
//    @Test
//    public void amountGrowToOneWhenWordFromSongInfoMeetMoreThanOnce(){
//        Map<String, Word> stringWordMap = new HashMap<>();
//        SongInfo songInfo = new SongInfo("name", singletonList("Мама, мама"), "url");
//        when(lineToWordsBreaker.getWords(songInfo)).thenReturn(stringWordMap);
//
//        Assertions.assertEquals(stringWordMap.size(),0);
//    }

}